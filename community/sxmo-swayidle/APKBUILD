# Maintainer: Stacy Harper <contact@stacyharper.net>
pkgname=sxmo-swayidle
pkgver=1.7
pkgrel=1
pkgdesc="Idle management daemon for Wayland - Sxmo version"
url="https://swaywm.org"
license="MIT"
arch="all"
options="!check" # no test suite
provides="swayidle"
makedepends="
	meson
	scdoc
	elogind-dev
	wayland-dev
	wayland-protocols
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/swaywm/swayidle/archive/$pkgver/swayidle-$pkgver.tar.gz
	0001-Allow-forked-processes-to-receive-signal.patch"
builddir="$srcdir"/swayidle-$pkgver

build() {
	abuild-meson \
		-Dlogind=enabled \
		-Dlogind-provider=elogind \
		. output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	mv "$pkgdir"/usr/share/fish/vendor_completions.d "$pkgdir"/usr/share/fish/completions
}

sha512sums="
26a48c510caaadb1ad694426e82880ab1547b7b0ef8ac62a2c2ee41c7b8cc0554f4925b0abdc6dc37f2501a171c0b6ae53c56b045b5c545093f3b0463aefcf24  swayidle-1.7.tar.gz
d4fa53f0d1babbc4597d443804ac45f96c4215c4dce50c813aacd4d38be0c072b1c1d307bf02722caf949a91a64f81fa7c059a0f2910ed9d295abcf90009b4c9  0001-Allow-forked-processes-to-receive-signal.patch
"
