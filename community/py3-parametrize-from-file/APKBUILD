# Contributor: Andy Hawkins <andy@gently.org.uk>
# Maintainer: Andy Hawkins <andy@gently.org.uk>
pkgname=py3-parametrize-from-file
pkgver=0.11.1
pkgrel=0
pkgdesc="Parametrize From File"
url="https://parametrize-from-file.readthedocs.io/en/latest/"
arch="noarch"
license="MIT"
depends="
	python3>=3.6
	py3-toml
	py3-yaml
	py3-nestedtext
	py3-tidyexc
	py3-more-itertools>=8.10
	py3-contextlib2
	py3-decopatch
	py3-voluptuous
	"
makedepends="
	py3-build
	py3-flit
	py3-pip
	"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-coveralls
	py3-numpy
	"
source="https://github.com/kalekundert/parametrize_from_file/archive/v$pkgver/parametrize_from_file-v$pkgver.tar.gz"
builddir="$srcdir/parametrize_from_file-$pkgver"

build() {
	python3 -m build --skip-dependency-check --no-isolation --wheel .
}

check() {
	PYTHONPATH="$PWD/build/lib" pytest
}

package() {
	local whl=dist/parametrize_from_file-$pkgver-py2.py3-none-any.whl
	pip3 install --no-deps --prefix=/usr --root="$pkgdir" "$whl"
}

sha512sums="
83e5b7797d7093cf44290559260b5570ef2ae63316dd40338c53237436e8f60d3dc240fe32bef5b2cdce94dbd526a141c53b1b8c555d1bf725321e2c6f0fefef  parametrize_from_file-v0.11.1.tar.gz
"
