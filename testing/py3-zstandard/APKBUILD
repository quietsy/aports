# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-zstandard
pkgver=0.16.0
pkgrel=0
pkgdesc="Python bindings to the Zstandard (zstd) compression library"
url="https://github.com/indygreg/python-zstandard"
license="BSD-3-Clause"
# armhf: tests fail (bus error)
# s390x: https://github.com/indygreg/python-zstandard/issues/105
arch="all !armhf !s390x"
depends="python3"
makedepends="python3-dev py3-setuptools py3-cffi"
checkdepends="py3-hypothesis"
source="$pkgname-$pkgver.tar.gz::https://github.com/indygreg/python-zstandard/archive/$pkgver.tar.gz"
builddir="$srcdir/python-zstandard-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --root="$pkgdir" --skip-build
}

sha512sums="
d016017db79d60a1bfd318968f595959bf7feacbd3d0fec4f1bd89f122cbc4923b4744b760332eae36e5deb0041336e2fddc7a165055b33c4d60eae5db5e0d34  py3-zstandard-0.16.0.tar.gz
"
