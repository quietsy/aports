# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=kopia
pkgver=0.10.1
pkgrel=0
pkgdesc="Fast and secure backup tool"
url="https://kopia.io/"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # tests fail with out of memory error
makedepends="go"
subpackages="$pkgname-bash-completion $pkgname-zsh-completion"
checkdepends="openssh-keygen"
source="https://github.com/kopia/kopia/archive/v$pkgver/kopia-$pkgver.tar.gz
	skip-docker-tests.patch
	"

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -ldflags "-s -w -X github.com/kopia/kopia/repo.BuildVersion=$pkgver"

	./kopia --completion-script-bash > $pkgname.bash
	./kopia --completion-script-zsh > $pkgname.zsh
}

check() {
	go test -tags testing ./...
}

package() {
	install -Dm755 kopia "$pkgdir"/usr/bin/kopia

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
86e502ce16e8756135af3b6b6bf9a768ae020e341ff1127096df921711d6afab77dd3c86941e85a887a750b305ca6ca214f32d26c6770128d4237f4758fd4d9c  kopia-0.10.1.tar.gz
6c1c8ca52d83c940c561f11adc18298147882b709810edd8c6560c8988ff1bd30dae2adba4c18055d283e7c2c655a6c6f10c3951829826d6fe5eea20c8cb821d  skip-docker-tests.patch
"
