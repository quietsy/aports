# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer:
pkgname=drone
pkgver=2.8.0
pkgrel=0
pkgdesc="Container-Native, Continuous Delivery Platform"
url="https://drone.io/"
license="custom"
arch="all"
makedepends="go"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/drone/drone/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	GO111MODULE=on go build -o bin/drone-server ./cmd/drone-server
}

check() {
	go test ./...
}

package() {
	install -Dm775 "$builddir"/bin/drone-server \
		"$pkgdir"/usr/bin/drone-server
	install -Dm 644 "$builddir"/LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

cleanup_srcdir() {
	go clean -modcache
	default_cleanup_srcdir
}

sha512sums="
a054694bcb8b945382cc5c0fc7db137a7c0bd57f4591ee9d62ece780d4226d7b64abc095d591e3392b99ef7e934af424db707c5bd8b856aaae8016e449d7dfba  drone-2.8.0.tar.gz
"
