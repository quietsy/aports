# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=pyright
pkgver=1.1.208
pkgrel=0
pkgdesc="Static type checker for Python"
url="https://github.com/microsoft/pyright"
license="MIT"
# riscv64: blocked by nodejs
# s390x: tests fail
arch="noarch !riscv64 !s390x"
depends="nodejs"
makedepends="npm"
subpackages="$pkgname-doc"
source="https://github.com/microsoft/pyright/archive/$pkgver/pyright-$pkgver.tar.gz"

prepare() {
	default_prepare
	npm ci
}

build() {
	cd packages/pyright
	npm run build
}

check() {
	cd packages/pyright-internal
	npm run test
}

package() {
	local destdir=/usr/lib/node_modules/pyright

	install -d \
		"$pkgdir"/usr/bin \
		"$pkgdir"/$destdir \
		"$pkgdir"/usr/share/doc/pyright

	cp -r docs/* LICENSE.txt "$pkgdir"/usr/share/doc/pyright

	cd packages/pyright
	cp -r dist index.js langserver.index.js package.json "$pkgdir"/$destdir

	ln -s $destdir/index.js "$pkgdir"/usr/bin/pyright
	ln -s $destdir/langserver.index.js "$pkgdir"/usr/bin/pyright-langserver
}

sha512sums="
2e34a1801ed2fa2eb162e805f43049c492104595a5a7f2f14a0ab4d6858825c8cc6792fd9515763fea90ff5b47a9c9df880ad3382e51c8173f31c6fc54ec5a4c  pyright-1.1.208.tar.gz
"
